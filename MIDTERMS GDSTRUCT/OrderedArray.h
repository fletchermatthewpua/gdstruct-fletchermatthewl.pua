#pragma once
#include <assert.h>

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		//your code goes after this line
		mArray[mNumElements] = value;
		mNumElements++;
		for (int i = 0; i < mNumElements; i++)
		{
			for (int j = 0; j < mNumElements - i - 1; j++)
			{
				if (mArray[j] > mArray[j + 1])
				{
					int temporaryNumber = mArray[j];
					mArray[j] = mArray[j + 1];
					mArray[j + 1] = temporaryNumber;
				}
			}
		}
		
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		//your code goes after this line
		int leftSide = 0;
		int rightSide = mMaxSize - 1;
		while (leftSide <= rightSide)
		{
			int midPoint = ((leftSide + rightSide) / 2);
			if (mArray[midPoint] == val)
			{
				return 1;
			}
			else if (val < mArray[midPoint])
			{
				rightSide = midPoint - 1;
			}
			else 
			{
				leftSide = midPoint + 1;
			}

			for (int i = 0; i < mNumElements; i++)
				if (mArray[i] == val)
					return i;
		}
		return -1;
	}
	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}
private:
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;



};