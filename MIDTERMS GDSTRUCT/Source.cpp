#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter size of dynamic arrays: ";
	int size;
	cin >> size;
	system("cls");

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	srand(time(NULL));
	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while (1)
	{
		cout << "Generated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		cout << "\nWhat do you want to do ?" << endl
			<< "[1]  Remove element at index " << endl
			<< "[2]  Search for element " << endl
			<< "[3] Expand and generate random values " << endl;
		int menu;
		cin >> menu;

		if (menu == 1) //Remove element
		{
			cout << "\n\nEnter index to remove : ";
			int removeIndex;
			cin >> removeIndex;
			if (removeIndex < unordered.getSize())
			{
				cout << "Element removed: " << removeIndex;
				unordered.remove(removeIndex);
				ordered.remove(removeIndex);

				cout << "\nUnordered: ";
				for (int i = 0; i < unordered.getSize(); i++)
					cout << unordered[i] << "   ";
				cout << "\nOrdered: ";
				for (int i = 0; i < ordered.getSize(); i++)
					cout << ordered[i] << "   ";
				cout << endl;
				system("pause");
			}
			else
			{
				cout << "Index is more than the current size!" << endl;
				cout << "Element removed: " << removeIndex;
				unordered.remove(removeIndex);
				ordered.remove(removeIndex);

				cout << "\nUnordered: ";
				for (int i = 0; i < unordered.getSize(); i++)
					cout << unordered[i] << "   ";
				cout << "\nOrdered: ";
				for (int i = 0; i < ordered.getSize(); i++)
					cout << ordered[i] << "   ";
				cout << endl;
				system("pause");
			}
		}
		else if (menu == 2)
		{
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			system("pause");
		}
		else if (menu == 3)
		{
			cout << "Input size of expansion: ";
			int sizeOfExpansion;
			cin >> sizeOfExpansion;
			unordered.expand();
			ordered.expand();
			for (int i = 0; i < sizeOfExpansion; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			
			cout << "\nArrays have been expanded: \n";
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			system("pause");
			system("cls");
		}
		else
		{
			system("cls");
		}

	}
}