#include <iostream>
#include <string>

using namespace std;

void computeSumOfDigitsOfNumber(int number, int sumOfDigits)
{
	if (number <= 0)
	{
		return;
	}
	cout << number % 10;

	sumOfDigits = sumOfDigits + number % 10;
	if (number <= 9)
	{
		cout << " = " << sumOfDigits << endl;
		cout << "Output = " << sumOfDigits << endl;
	}
	else 
	{
		cout << " + ";
	}
	number = number / 10;

	computeSumOfDigitsOfNumber(number, sumOfDigits);
}

void printFibonnaciSequence (int number, int firstNumber, int secondNumber, int printNumber)
{
	if (printNumber >= number)
	{
		return;
	}

	if (firstNumber == 0 && secondNumber == 1) // displaying 0 and 1
	{
		cout << firstNumber << " " << secondNumber << " ";
	}

	cout << printNumber << " ";
	firstNumber = secondNumber;
	secondNumber = printNumber;
	printNumber = firstNumber + secondNumber;

	printFibonnaciSequence(number, firstNumber, secondNumber, printNumber);
}

void checkIfPrimeOrNot(int number, int& ifPrimeOrNot , int count)
{
	if (count >= 10)
	{
		return;
	}

	if (number == 0) // 0 is not a Prime Number
	{
		ifPrimeOrNot = 1;
	}
	else if (number == 1 || number == 2) // 1 and 2 are Prime Numbers
	{
		ifPrimeOrNot = 0;
		return;
	}
	else if (number % count == 0) // Not Prime
	{
		ifPrimeOrNot = 1;
	}
	count++;
	checkIfPrimeOrNot(number, ifPrimeOrNot, count);
}

void main()
{
	int numberInput = 0;
	int sum = 0;

	// Sum Of Digits
	cout << "Sum Of the Digits: " << endl << "Input a number: ";
	cin >> numberInput;

	computeSumOfDigitsOfNumber(numberInput, sum);
	system("pause");
	system("cls");

	//Fibonacci 
	cout << " Print the Fibonnaci sequence up to the number: ";
	cin >> numberInput;

	printFibonnaciSequence(numberInput, 0, 1, 1);
	cout << endl;
	system("pause");
	system("cls");

	//Check if Prime Or Not
	cout << "Prime number checker: " << endl << "Input a number: ";
	cin >> numberInput;

	checkIfPrimeOrNot(numberInput);
	system("pause");
}