#include <string>
#include <iostream>
#include <vector>
#include "UnorderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	int userSize = 0;
	int userRemove = 0;
	char userInput;
	int linearSearchValue = 0;
	cout << "How many integers do you want? ";
	cin >> userSize;

	UnorderedArray<int> numbers(userSize);

	srand(time(NULL));
	for (int i = 0; i < userSize; i++) // Add Random Numbers
	{
		numbers.push(rand() % 100 + 1);
	}

	cout << "Unordered Array: " << endl;

	for (int i = 0; i < numbers.getSize(); i++) // Display Numbers
	{
		cout << numbers[i] << endl;
	}

	cout << "Do you wish to remove an integer? (y/n) ";
	cin >> userInput;
	

	if (userInput == 'y')
	{
		cout << "What posistion of the number do you wish to remove? ";
		cin >> userRemove;
		numbers.remove(userRemove - 1);
		for (int i = 0; i < numbers.getSize(); i++)
		{
			cout << numbers[i] << endl;
		}
	}
	else 
	{
	}

	//Linear Search
	cout << "Linear Search: " << endl << "Input a number: ";
	cin >> linearSearchValue;

	int index = numbers.linearSearch(numbers, linearSearchValue);
	if (index == -1)
		cout << "The number is not present in the array" << endl;
	else
		cout << "The number is found at the position " << index << endl;

	system("pause");
}