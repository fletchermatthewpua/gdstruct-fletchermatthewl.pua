#include <string>
#include <iostream>

using namespace std;

// GDSTRUCT SW #3

void main()
{
	string nameOfGuild;
	int sizeOfGuild;
	int operationInput = 0;
	int selectMember = 0;
	string renameMemeberName;

	while (1)
	{
		// Declaring name for the guild
		cout << "What is the name of your guild? ";
		cin >> nameOfGuild;
		
		// Declaring initial size of the guild
		cout << "\nHow many members do you wish to be in your guild ? ";
		cin >> sizeOfGuild;


		string *dynamicMembers = new string[sizeOfGuild];

		// Input usernames of members
		for (int i = 0; i < sizeOfGuild; i++)
		{
			cout << "\nWhat is the name of the member " << i << ": ";
			cin >> dynamicMembers[i];
		}
		system("cls");

		while (1)
		{
			//Operations
			cout << "[1] Print every member " << endl << "[2] Rename a member " << endl << "[3] Add a member " << endl << "[4] Delete a member" << endl;
			cin >> operationInput;

			if (operationInput == 1)
			{
				//Display all members
				cout << "\nMembers: " << endl;
				for (int i = 0; i < sizeOfGuild; i++)
				{
					cout << dynamicMembers[i] << endl;
				}
				system("pause");
				system("cls");
			}
			else if (operationInput == 2)
			{
				//Rename a member
				cout << "Which member do you want to rename: \n";
				for (int i = 0; i < sizeOfGuild; i++)
				{
					cout << "[" << i << "] " << dynamicMembers[i] << endl;
				}
				cin >> selectMember; 
				cout << "What is the new name of the member: ";
				cin >> renameMemeberName;
				dynamicMembers[selectMember] = renameMemeberName;

				cout << "\nMembers: " << endl;
				for (int i = 0; i < sizeOfGuild; i++)
				{
					cout << dynamicMembers[i] << endl;
				}

				system("pause");
				system("cls");
			}
			else if (operationInput == 3)
			{
				//Add a member
				string* tempDynamicMembers = new string[sizeOfGuild + 1];
				for (int i = 0; i < sizeOfGuild; i++)
				{
					tempDynamicMembers[i] = dynamicMembers[i];
				}
				sizeOfGuild++;
				delete[] dynamicMembers;
				dynamicMembers =tempDynamicMembers;

				cout << "Name of the new member: ";
				cin >> dynamicMembers[sizeOfGuild - 1];

				cout << "\nMembers: " << endl;
				for (int i = 0; i < sizeOfGuild; i++)
				{
					cout << dynamicMembers[i] << endl;
				}

				system("pause");
				system("cls");
			}
			else if (operationInput == 4)
			{
				//Delete a member
				cout << "Which member do you want to delete: \n";
				for (int i = 0; i < sizeOfGuild; i++)
				{
					cout << "[" << i << "] " << dynamicMembers[i] << endl;
				}
				cin >> selectMember;
				dynamicMembers[selectMember] = "[EMPTY SLOT]";

				cout << "\nMembers: " << endl;
				for (int i = 0; i < sizeOfGuild; i++)
				{
					cout << dynamicMembers[i] << endl;
				}
				system("pause");
				system("cls");
			}
		}
	}
}