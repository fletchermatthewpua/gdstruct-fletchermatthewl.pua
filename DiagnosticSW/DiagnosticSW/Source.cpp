#include <iostream>
#include <conio.h>
#include <time.h>
using namespace std;

int randNumber()
{
	return rand()% 69 + 1;
}

int main()
{
	srand(time(NULL));
	int array[10];
	int ascOrDes = 0;
	int userNumber = 0;

	for (int i = 0; i < 10; i++)
	{
		array[i] = randNumber();
	}

	cout << "Random Number: \n";

	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << endl;
	}

	for (int i = 0; 1; i++)
	{
	cout << "How do you want it arrange?" << endl << "[1] ASCENDING" << endl << "[2] DESCENDING" << endl;
		cin >> ascOrDes;
		if (ascOrDes == 1)
		{
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 10 - i - 1; j++)
				{
					if (array[j] > array[j + 1])
					{
						int temporaryNumber = array[j];
						array[j] = array[j + 1];
						array[j + 1] = temporaryNumber;
					}
				}
			}
			break;
		}
		else if (ascOrDes == 2)
		{
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					if (array[j] < array[j + 1])
					{
						int temporaryNumber = array[j];
						array[j] = array[j + 1];
						array[j + 1] = temporaryNumber;
					}
				}
			}
			break;
		}
	}
	cout << "Random Number: \n";

	for (int i = 0; i < 10; i++)
	{
		cout << array[i] << endl;
	}

	//Linear Search
	cout << "Pick any Number: ";
	cin >> userNumber;
	for (int i = 0, numberPosistion = 0; i < 10; i++)
	{
		numberPosistion++;
		if (array[i] == userNumber)
		{
			cout << "The number is found.\n" << "Number Of Steps it took : " << numberPosistion << endl;
		}
		else if (i == 9)
		{
			cout << "Number not found." << endl;
		}
	}
	
	system("pause");
	return 0;
}